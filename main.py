#https://github.com/hwchase17/langchain-streamlit-template/blob/master/main.py
#https://medium.com/@rubentak/talk-to-your-pdf-files-in-a-pinecone-vector-databases-with-gpt-4-a-step-by-step-tutorial-1632cf7aa041

"""Python file to serve as the frontend"""
import streamlit as st
from streamlit_chat import message
import pinecone
import os

from langchain.chains import ConversationChain
from langchain.llms import OpenAI

from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import Pinecone

# PDF Loaders. If unstructured gives you a hard time, try PyPDFLoader
from langchain.document_loaders import UnstructuredPDFLoader, OnlinePDFLoader, PyPDFLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter

from dotenv import load_dotenv
load_dotenv()

OPENAI_API_KEY=os.environ["OPENAI_API_KEY"]
pc_api_key=os.environ["PINECONE_ENV"]
pc_environment=os.environ["PINECONE_API_KEY"]
pc_index=os.environ["PINECONE_INDEX_NAME"]
OPENAI_API_KEY = os.environ.get('OPENAI_API_KEY', 'sk-...')

embeddings = OpenAIEmbeddings()

# create a pinecone index
#pinecone.create_index("python-index", dimension=1536, metric="cosine")

# we create a new index
#pinecone.create_index(
#        name=pc_index,
#        metric='dotproduct',
#        dimension=len(res[0]) # 1536 dim of text-embedding-ada-002
#)

# initialize pinecone
pinecone.init(
        api_key=pc_api_key,  # find api key in console at app.pinecone.io
        environment=pc_environment  # find next to api key in console
)

#pinecone.init(
#    api_key=str(os.environ['PINECONE_API_KEY']),  
#    environment=str(os.environ['PINECONE_ENV'])  
#)

pc_index = str(os.environ['PINECONE_INDEX_NAME'])

# create a loader
loader = PyPDFLoader("valuation.pdf")

#The following two loaders can also be used for other PDF files:
#loader = UnstructuredPDFLoader("../data/summary_strategy.pdf")
#loader = OnlinePDFLoader("...")

# load your data
data = loader.load()

text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=100)
texts = text_splitter.split_documents(data)

docsearch = Pinecone.from_texts([t.page_content for t in texts], embeddings, index_name=pc_index)

#If you already have an index, you can load it like this
#docsearch = Pinecone.from_existing_index(index_name=pc_index, embeddings)

st.stop() #先创建Pinecone向量数据库

#def load_chain():
#    """Logic for loading the chain you want to use should go here."""
#    llm = OpenAI(temperature=0)
#    chain = ConversationChain(llm=llm)
#    return chain

def load_chain():
    docsearch = Pinecone.from_existing_index(index_name=pc_index, embeddings)
    return docsearch

chain = load_chain()

# From here down is all the StreamLit UI.
st.set_page_config(page_title="LangChain Demo", page_icon=":robot:")
st.header("LangChain Demo")
st.write("这个是测试Langchain框架下，使用Pinecone直接装载调用index的向量数据库进行问答")
st.write("源代码位于Gitlab/binqiangliu|bq.liu@qq.com")
st.write("App部署在Rd.com的bq.liu@qq.com账户下|GoChatDocStreamlit")

if "generated" not in st.session_state:
    st.session_state["generated"] = []

if "past" not in st.session_state:
    st.session_state["past"] = []


def get_text():
    input_text = st.text_input("You: ", "Hello, how are you?", key="input")
    return input_text


user_input = get_text()

#if user_input:
#   output = chain.run(input=user_input)

if user_input:
   docs = chain.similarity_search(user_input)
   output = docs[0].page_content

st.session_state.past.append(user_input)
st.session_state.generated.append(output)

if st.session_state["generated"]:

    for i in range(len(st.session_state["generated"]) - 1, -1, -1):
        message(st.session_state["generated"][i], key=str(i))
        message(st.session_state["past"][i], is_user=True, key=str(i) + "_user")
